const factory = (op, data)  => {
    if (op === "+") {
      
        return data.a + data.b;
     
    }
  
    if (op === "-") {
     
        return data.a - data.b;
     
    }
  
    if (op === "*") {
      
        return data.a * data.b;
      
    }
  
    if (op === "/") {
      
        return data.a / data.b;
     
    }
  }
  


  console.log(factory("+", { a: 2, b: 2 }));
  console.log(factory("-", { a: 2, b: 2 }));
  console.log(factory("*", { a: 2, b: 2 }));
  console.log(factory("/", { a: 2, b: 2 }));
  